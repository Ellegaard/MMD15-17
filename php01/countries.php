<head>
    <title>Lande</title>
    <meta charset="utf-8">
    <!-- Reference til bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>


<?php
    /*
     * Array
     * Opsæt et accosiativt array der indeholder byer og deres hovedstader.
     * Der skal minimum være 20 lande og deres respektive hovedstader i arrayet.
     * "Italy"=>"Rome"
     * Når Arrayet er opsat, skal du udskrive arrayet tre gange.
     * 1. gang skal arrayet udskrives i den rækkefølge, som angivet i arrayet.
     * 2. gang skal arrayet udskrives, således, at landenavne er sorteret efter forbogstav. Lande med A i forbogstav skal komme først.
     * 3. gang skal arrayet udskrives, således, at bynavnen er sorteret efter forbogstav.
     * Få hjælp her: http://php.net/manual/en/array.sorting.php
     * I mappen ligger landeOgByer.png hvor du kan se udskriften for arrayet der ikke er sorteret.
     */

    $countries = array( "Italy"=>"Rome", "Luxembourg"=>"Luxembourg", "Belgium"=>"Brussels", "Denmark"=>"Copenhagen", "Finland"=>"Helsinki", "France"=>"Paris", "Slovakia"=>"Bratislava", "Slovenia"=>"Ljubljana", "Germany"=>"Berlin", "Greece"=>"Athens", "Ireland"=>"Dublin", "Netherlands"=>"Amsterdam", "Portugal"=>"Lisbon", "Spain"=>"Madrid", "Sweden"=>"Stockholm", "United Kingdom"=>"London", "Cyprus"=>"Nicosia", "Lithuania"=>"Vilnius", "Czech Republic"=>"Prague", "Estonia"=>"Tallin", "Hungary"=>"Budapest", "Latvia"=>"Riga", "Malta"=>"Valetta", "Austria"=>"Vienna", "Poland"=>"Warsaw");

    $countries += array("Korea"=>"Seoul", "Vietnam"=>"Hanoi");

   

    
?>
    <body>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h2>Liste</h2>
                <?php
                   foreach ($countries as $key => $value) {
                        echo "<li>Land: ".$key. " Hovedstad: ".$value. "</li>";
                    }
                ?>
            </div>
            <div class="col-md-4">
                <h2>Efter Land</h2>
                <?php
                    ksort($countries);  //Sorterer alfabetisk efter land - kigger på $key   
                    foreach ($countries as $key => $value) {
                        echo "<li>Land: ".$key. " Hovedstad: ".$value. "</li>";
                    }
                ?>
            </div>
            <div class="col-md-4">
                <h2>Efter Hovedstad</h2>
                <?php
                    asort($countries); //Sorterer alfabeteisk efter hovedstad - kigger på $value
                    foreach ($countries as $key => $value) {
                        echo "<li>Land: ".$key. " Hovedstad: ".$value. "</li>";
                    }
                ?>
            </div>
        </div>
    </div>
<body>


<?php

    /*
     * Ekstra opgave
     * Tilføj flere elementer til arrayet. Brug array_push()
     */

?>