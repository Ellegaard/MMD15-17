<!-- Variabler-->
<?php
    //Datatyper og variabler. Dette dokument indlejres i det efterfølgende

    //tekststreng (string)
    $firstName = "Mette";

    //tekstreng (string)
    $lastName = "Hansen";

    //nummerisk heltal (integer)
    $age = 27;

    //boolean (sandt/falsk)
    $inRelationship = true;

    //tekststreng (string)
    $work = "Studerende";

    //tekststreng (string)
    $workPlace = "Erhversakademi Dania, Skive";

    //array (en række)
    $hobbies = ["Katte", "Skriftligt Rollespil", "World of Warcraft"];
?>