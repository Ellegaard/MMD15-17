<!doctype>

<head>
    <title>Min kontaktside</title>
    <meta charset="utf-8">
    <!-- Reference til bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Include af php dokument-->
    <?php
    include("opgave_1_data.php");
    ?>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h2>Resume</h2>
                <?php
                    echo "<strong>Fulde navn: </strong>" .$first_name." ".$last_name."<br>";
                    echo "<strong>Alder: </strong>".$age."<br>";
                    echo "<strong>E-mail: </strong>".$email."<br>";
                    echo "<strong>Mobil: </strong>".$phone."<br>";
                ?>
            </div>
            <div class="col-md-3">
                <h2>Præferencer</h2>
                <?php
                    foreach ($preferences as $value) {  //foreach loop der kigger på hvert element i array $preferences
                        echo "$value <br>";
                    }
                ?>
            </div>
            <div class="col-md-3">
                <h2>Portefølje!</h2>
                <?php
                    foreach ($jobs as $location => $description) {
                        echo "Hos ".$location . " lavede jeg " . $description . "<br>";
                    }
                ?>
            </div>
            <div class="col-md-3">
                <h2>Kompetencer</h2>
                <?php
                        foreach ($competencies as $comp) {  //foreach løkke - sætter $competencies til at tælle som $comp
                            if(is_array($comp)){ //hvis der kommer et array med $comp - array i array
                                foreach ($comp as $c) { //foreach løkke der sætter $comp til at tælle som $c
                                    echo "<li>" . $c . "</li>"; //udskriv $c som et listelement
                                }
                            }else { //hvis ikke der findes et array
                                echo "<li>" . $comp . "</li>"; //udskriv $comp som listeelement
                            }
                        }
                    ?>    
            </div>
        </div>
    </div>
<body>