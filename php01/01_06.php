<!--Variabler og datatyper-->

<?php

/* datatyper
    * - Integer
    * - Float
*/

$a = 1234;                          // decimal number
var_dump($a);

$a = -123;                          // a negative number
var_dump($a);       

$a = 0123;                          //octal number (equivalent to 83 decimal)
var_dump($a);



$a = 1.3; 
var_dump($a);

$larger_number = 214783647;
var_dump($larger_number);           // int(214783647)

$larger_number = 214783648;
var_dump($larger_number);           // float(214783648)

$million = 1000000;
$larger_number = 50000 * $million;
var_dump($larger_number);           //float(50000000000)

var_dump(25/7);                     // float(3.5714285714286)

var_dump((int)(25/7));              //int(3)

var_dump(round(25/7));              //float(4)



?>