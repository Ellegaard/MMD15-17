<?php
    /* Dette dokument skal indeholde de dataelementer der indgår i html siden.
     * Følgende dataelementer skal som minimum være udpenslet i dette dokument.:
     * - email 
     * - phone
     * - preferences
     * - jobs
     * - competencies
     *
     * Det vil være naturligt at lade ovenstående elementer være 
     */

     $first_name = "Mette";
     $last_name = "Hansen";
     $age = 27;
     $email = "tumling89@hotmail.com";
     $phone = 30482908;

     $preferences = array('Programmering','Kommunikation','Kultur');

     $jobs = array('Kvickly Xtra, Viby' => 'Kassebetjening','Hydro Texaco, Viby' => 'Tankpasser','Lagkagehuset' => 'Salg af brød','CM Polering' => 'Design af visuel identitet');

     
     $competencies = array('Programmering',array('PHP','JavaScript','HTML','CSS'), array('JQuery Mobile','Bootstrap','Foundation'));

?>