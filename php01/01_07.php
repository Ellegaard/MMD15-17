<?php
    /*Datatyper
        String eller tekststrengværdier
    */

    $a = "Dette er en tekststreng <br>";
    echo $a;

    $a = "Dette er en tekststreng <br>";
    echo $a;

    $name = "Mette";
    echo "Hej, $name";
    echo "<br>";
    echo "Hej, $name";

    $a = "<p>Dette er en tekststreng der inderholder afsnitselementer</p>";
    echo $a;

    date_default_timezone_set("Europe/Copenhagen");
    $a = "<div>Dato i dag er: " . date("d. m Y") . " og kl. er: " . date("h:i:s");
    echo $a; 


    // Teste om to strenge er ens
    echo "<br>";
    echo "<br>";
    echo "Er de to variabler ens? <br>";
    $a = "abc";
    $b = "abc";


    if($a == $b)
    {
        echo 'Ja, $a og $b er ens <br>';
    }
?>