<?php
    /*
     * Opgave 03_02
     * 
     * Metoden metoden extractValuesFromArray skal kunne hente værdierne fra et indexeret array. 
     * Brug den indbyggede metode list() til at udtrække værdierne.
     * Se kapitel - Array -> Extracting multiple values.
     */
    
    class Person //Object Person start
    {
        function extractValuesFromArray() //function start
        {
            $someRandomPerson = array("Fred", 35, "Betty"); //function med et array, der indeholder 3 værdier
            list($name, $age, $wife) = $someRandomPerson; // $name er 'Fred', $age er 35, $wife er 'Betty'
            //return [$name, $age, $wife]; //returnerer tre variabler, $name, $age og $wife
            return $name . ' ' . $age . " " . $wife; //returnerer tre variabler, $name, $age og $wife
        } //function end
    } //Object Person end
    $person = new Person; //variablen $person kalder nu et object Person. Aktiveres her ved at skrive variablen $person
    echo $person->extractValuesFromArray(); //udskriver hvad der bliver returneret fra functionen i objektet
?>  